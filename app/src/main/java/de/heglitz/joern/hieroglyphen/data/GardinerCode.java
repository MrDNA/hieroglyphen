package de.heglitz.joern.hieroglyphen.data;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.regex.Pattern;

public class GardinerCode implements Comparable<GardinerCode>{
    String category,variant;
    int number;

    public GardinerCode(String category, int number, String variant){
        this.category=category;
        this.number=number;
        this.variant=variant;
    }
    public GardinerCode(String category, int number){
        this(category,number,null);
    }

    public static GardinerCode parse(String string) throws GardinerParserException{
        Log.d("parser","parsing "+string);
        String cat="",var="";
        int num=0,section=1;
        for (;string.length()>0;string=string.substring(1)){
            char c=string.charAt(0);
            if(section==1&&c>='A'&&c<='Z'){
                cat+=c;
            }
            else if (section<=2&&c>='0'&&c<='9'){
                section=2;
                num*=10;
                num+=((int)c)-48;
            }
            else if(section>=2&&c>='a'&&c<='z'){
                section=3;
                var+=c;
            }
            else throw new GardinerParserException("unexpected symbol: "+c);
        }
        GardinerCode gc=new GardinerCode(cat,num,var.length()>0?var:null);
        Log.d("parser","parsed to "+gc.toString());
        return gc;
    }

    @Override
    public int compareTo(@NonNull GardinerCode gardinerCode) {
        if(gardinerCode.category.equals(category)){
            if(gardinerCode.number==number){
                if (variant==gardinerCode.variant) return 0;
                else if(variant==null) return -1;
                else if(gardinerCode.variant==null) return 1;
                else if(gardinerCode.variant.length()>1)return 1;
                else if(variant.length()>1)return -1;
                else return gardinerCode.variant.charAt(0)>variant.charAt(0)?1:-1;
            }
            else return gardinerCode.number-number;
        }
        else if(gardinerCode.category.length()>1)return 1;
        else if(category.length()>1)return -1;
        else return gardinerCode.category.charAt(0)>category.charAt(0)?1:-1;
    }

    @Override
    public String toString() {
        String string=category+Integer.toString(number);
        if(variant!=null) string+=variant;
        return string;
    }

    public static class GardinerParserException extends Exception{
        protected GardinerParserException(String msg){
            super(msg);
        }
    }
}
