package de.heglitz.joern.hieroglyphen.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import de.heglitz.joern.hieroglyphen.R;
import de.heglitz.joern.hieroglyphen.data.Hieroglyphe;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent=getIntent();
        Hieroglyphe hieroglyphe=(Hieroglyphe) intent.getParcelableExtra("clickedOn");

        ImageView iv=findViewById(R.id.detailImage);
        //SVG svg= SVGParser.getSVGFromInputStream(hieroglyphe.getImageStream());
        //iv.setImageDrawable(svg.createPictureDrawable());


        Log.d("detail","desc: "+hieroglyphe.description);
        TextView text=findViewById(R.id.detailTitle);
        text.setText(hieroglyphe.description);

        text=findViewById(R.id.detailKat);
        text.setText(hieroglyphe.id.toString());

        text=findViewById(R.id.detailPhono);
        text.setText(hieroglyphe.phono);

        text=findViewById(R.id.detailDet);
        text.setText(hieroglyphe.determ);

        text=findViewById(R.id.detailIdeo);
        text.setText(hieroglyphe.ideo);

    }
}
