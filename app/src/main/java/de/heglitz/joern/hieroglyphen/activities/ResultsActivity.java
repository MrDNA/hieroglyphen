package de.heglitz.joern.hieroglyphen.activities;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import de.heglitz.joern.hieroglyphen.R;
import de.heglitz.joern.hieroglyphen.data.Hieroglyphe;
import de.heglitz.joern.hieroglyphen.database.DatabaseAccess;

public class ResultsActivity extends AppCompatActivity {


    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("search", "onNewIntent: invoked");
        setIntent(intent);
        handleIntent(intent, (ListView) findViewById(R.id.list));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("search", "onCreate: invoked");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        handleIntent(getIntent(),(ListView) findViewById(R.id.list));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.optbutton){
            Log.i("main","gedrückt");
            onSearchRequested();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void handleIntent(Intent intent, ListView list){
        if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("search", "searched for: " + query);
            DatabaseAccess db=DatabaseAccess.getInstance(this);
            db.open();
            Hieroglyphe[] ergb=db.searchFor(query);
            db.close();
            /*
            if ("mann".equalsIgnoreCase(query)) {
                ergb = new Hieroglyphe[]{new Hieroglyphe(R.drawable.ic_a1, Hieroglyphe.Category.A, "A1", "Mann", "", "", "")};
            } else if ("frau".equalsIgnoreCase(query)) {
                ergb = new Hieroglyphe[]{new Hieroglyphe(R.drawable.ic_b1, Hieroglyphe.Category.A, "B1", "Frau", "", "", "")};
            } else {
                ergb = new Hieroglyphe[]{new Hieroglyphe(R.drawable.ic_a1, Hieroglyphe.Category.A, "A1", "Mann", "", "", ""), new Hieroglyphe(R.drawable.ic_b1, Hieroglyphe.Category.A, "B1", "Frau", "", "", "")};
            }
            */
            final ResultListAdapter adapter = new ResultListAdapter(this, ergb);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Hieroglyphe selectedH=adapter.getItem(i);
                    Log.d("list","clicked on "+selectedH.id);
                    Intent intent =new Intent(ResultsActivity.this,DetailActivity.class);
                    intent.putExtra("clickedOn",selectedH);

                    startActivity(intent);
                }
            });
        }
    }
}
