package de.heglitz.joern.hieroglyphen.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import de.heglitz.joern.hieroglyphen.data.GardinerCode;
import de.heglitz.joern.hieroglyphen.data.Hieroglyphe;

/**
 * Created by Jörn on 03.03.2018.
 */

public class DatabaseAccess {

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;


    private DatabaseAccess(Context context) {
        this.openHelper = new DataBaseHelper(context);
    }

    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getReadableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public Hieroglyphe[] searchFor(String query){
        Set<Hieroglyphe> ergebnisse=new TreeSet<Hieroglyphe>();
        //Suche nach Gardiner Code
        Cursor c=database.query(true,"hieroglyphs",new String[]{"svg","category","id","description","determ","ideo","phono"},"id LIKE ?",new String[]{"%"+query+"%"},null,null,null,null);
        Log.d("SUCHE","Ergebnisse für Gardiner Code: "+c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            try {
                ergebnisse.add(new Hieroglyphe(c.getBlob(0), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            }
            catch (GardinerCode.GardinerParserException e){
                Log.e("SUCHE",e.getMessage());
                e.printStackTrace();
            }
            c.moveToNext();
        }
        c.close();


        //Suche nach Beschriebung
        c=database.query(true,"hieroglyphs",new String[]{"svg","category","id","description","determ","ideo","phono"},"description LIKE ?",new String[]{"%"+query+"%"},null,null,null,null);
        Log.d("SUCHE","Ergebnisse für Description: "+c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            try {
                ergebnisse.add(new Hieroglyphe(c.getBlob(0), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            }
            catch (GardinerCode.GardinerParserException e){
                Log.e("SUCHE",e.getMessage());
                e.printStackTrace();
            }
            c.moveToNext();
        }
        c.close();

        //Suche nach Determ
        c=database.query(true,"hieroglyphs",new String[]{"svg","category","id","description","determ","ideo","phono"},"determ LIKE ?",new String[]{"%"+query+"%"},null,null,null,null);
        Log.d("SUCHE","Ergebnisse für Determ: "+c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            try {
                ergebnisse.add(new Hieroglyphe(c.getBlob(0), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            }
            catch (GardinerCode.GardinerParserException e){
                Log.e("SUCHE",e.getMessage());
                e.printStackTrace();
            }
            c.moveToNext();
        }
        c.close();

        //Suche nach ideogram
        c=database.query(true,"hieroglyphs",new String[]{"svg","category","id","description","determ","ideo","phono"},"ideo LIKE ?",new String[]{"%"+query+"%"},null,null,null,null);
        Log.d("SUCHE","Ergebnisse für ideogarm: "+c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            try {
                ergebnisse.add(new Hieroglyphe(c.getBlob(0), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            }
            catch (GardinerCode.GardinerParserException e){
                Log.e("SUCHE",e.getMessage());
                e.printStackTrace();
            }
            c.moveToNext();
        }
        c.close();

        //Suche nach phonogram
        c=database.query(true,"hieroglyphs",new String[]{"svg","category","id","description","determ","ideo","phono"},"phono LIKE ?",new String[]{"%"+query+"%"},null,null,null,null);
        Log.d("SUCHE","Ergebnisse für phonogram: "+c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            try {
                ergebnisse.add(new Hieroglyphe(c.getBlob(0), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6)));
            }
            catch (GardinerCode.GardinerParserException e){
                Log.e("SUCHE",e.getMessage());
                e.printStackTrace();
            }
            c.moveToNext();
        }
        c.close();


        Hieroglyphe[] ret=new Hieroglyphe[ergebnisse.size()];
        ret=ergebnisse.toArray(ret);
        return ret;

    }


}
