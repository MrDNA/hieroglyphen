package de.heglitz.joern.hieroglyphen.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by Jörn on 03.03.2018.
 */

public class DataBaseHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "hieroglyphen.db";
    private static final int DATABASE_VERSION = 1;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

}
