package de.heglitz.joern.hieroglyphen.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import de.heglitz.joern.hieroglyphen.R;
import de.heglitz.joern.hieroglyphen.data.Hieroglyphe;

/**
 * Created by jrand on 15.02.2018.
 */

public class ResultListAdapter extends ArrayAdapter<Hieroglyphe>{

    private Context context;
    private Hieroglyphe[] data;

    public ResultListAdapter(@NonNull Context context, Hieroglyphe[] data) {
        super(context, -1, data);
        this.context=context;
        this.data=data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item=inflater.inflate(R.layout.item_list,parent,false);
        ImageView image=item.findViewById(R.id.icon);
        //Log.d("listadapterview","Hierogylphe: "+data[position].id);
        //SVG svg= SVGParser.getSVGFromInputStream(data[position].getImageStream());
        //image.setImageDrawable(svg.createPictureDrawable());
        TextView cate=item.findViewById(R.id.category);
        cate.setText(data[position].id.toString());
        TextView descr=item.findViewById(R.id.description);
        descr.setText(data[position].description);

        return item;
    }
}
