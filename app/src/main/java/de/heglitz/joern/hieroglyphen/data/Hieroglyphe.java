package de.heglitz.joern.hieroglyphen.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Created by jrand on 15.02.2018.
 */

public class Hieroglyphe implements Parcelable,Comparable<Hieroglyphe>{

    public final String description,determ,ideo,phono;
    public GardinerCode id;
    public final byte[] image;

    public Hieroglyphe(byte[] image,GardinerCode id,String description,String determinantiv,String ideogram,String phonogram){
        this.description=description;
        determ=determinantiv;
        ideo=ideogram;
        phono=phonogram;
        this.image=image;
        this.id=id;
    }

    public Hieroglyphe(byte[] image,String id,String description,String determinantiv,String ideogram,String phonogram)throws GardinerCode.GardinerParserException{
        this(image,GardinerCode.parse(id),description,determinantiv,ideogram,phonogram);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Hieroglyphe){
            return ((Hieroglyphe)obj).id.compareTo(this.id)==0;
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        String[] strings={description,determ,ideo,phono,id.toString()};
        parcel.writeStringArray(strings);
        parcel.writeInt(image.length);
        parcel.writeByteArray(image);
    }

    public static final Parcelable.Creator<Hieroglyphe> CREATOR=new Parcelable.Creator<Hieroglyphe>(){
        public Hieroglyphe createFromParcel(Parcel p){
            return new Hieroglyphe(p);
        }

        @Override
        public Hieroglyphe[] newArray(int i) {
            return new Hieroglyphe[i];
        }
    };

    private Hieroglyphe(Parcel p){
        String[] strings=new String[5];
        p.readStringArray(strings);
        int imageSize=p.readInt();
        image=new byte[imageSize];
        p.readByteArray(image);
        description=strings[0];
        determ=strings[1];
        ideo=strings[2];
        phono=strings[3];
        try{
        id=GardinerCode.parse(strings[4]);
        }
        catch (GardinerCode.GardinerParserException e){
            Log.e("hieroglyphe",e.getMessage());
            e.printStackTrace();
        }

    }

    public InputStream getImageStream(){
        return new ByteArrayInputStream(image);
    }

    @Override
    public int compareTo(@NonNull Hieroglyphe hieroglyphe) {
        return id.compareTo(hieroglyphe.id)==0?0:1;
    }


    public enum Category{
        A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,Aa
    }
}
